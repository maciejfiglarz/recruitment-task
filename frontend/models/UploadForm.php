<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $data;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsx', 'message'=>'Wybierz plik!'],
        ];
    }
  
    public function upload(): array
    {
        $data = ['status' => false, 'fileName' => ''];
        if ($this->validate()) {
            $path = Yii::$app->getBasePath() . '/data/uploads/';
            $fileName = str_replace(' ', '', $this->imageFile->baseName);
            $data['fileName'] = $fileName;
            $data['status'] = true;
            $this->imageFile->saveAs($path . $fileName . '.' . $this->imageFile->extension);
        }
        return $data;
    }
}
