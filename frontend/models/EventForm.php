<?php

namespace app\models;

use Yii;
use yii\base\Model;

class EventForm extends Model
{
    public $events;


    public function rules()
    {

        return [
            [['events'], 'required','message'=>'Wypełnij to pole!'],
            ['events', 'integer','message'=>'Wpisz liczbę!'],
        ];
    }

}