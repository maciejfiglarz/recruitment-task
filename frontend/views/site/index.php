<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<div class="site-index">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>


<div class="form-group">
    <?= $form->field($model, 'imageFile')->fileInput(['class' => 'form-control-file','on'=>'my-scenario']);  ?>
</div>

    <?= Html::submitButton('Wyślij', ['class' => 'btn btn-info']) ?>

<?php ActiveForm::end() ?>
</div>
