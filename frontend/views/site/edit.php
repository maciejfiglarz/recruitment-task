<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="container">
    <section class="section section-edit">

        <?php $form = ActiveForm::begin(); ?>
        <table class="table">
            <?php foreach ($rows as $indexRow => $row) : ?>
                <tr>

                    <?php foreach ($row as  $indexCell => $cell) : ?>
                        <?php $name = $indexRow . '_' . $indexCell; ?>
                        <?php if ($indexRow == 0) : ?>

                            <th>
                                <?= Html::encode("{$cell}") ?>
                                <?= $form->field($editForm, $name)->input('hidden')->label(false) ?>
                            </th>

                        <?php else : ?>
                            <td>
                                <?php if ($indexCell == 0) : ?>
                                    <?= Html::encode("{$cell}") ?>
                                    <?= $form->field($editForm, $name)->input('hidden')->label(false) ?>
                                <?php else : ?>
                                    <?= $form->field($editForm, $name)->input('text')->label(false) ?>
                                <?php endif; ?>
                            </td>


                        <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </table>

        <?= Html::submitButton('Wyślij', ['class' => 'btn btn-primary']) ?>

        <?php ActiveForm::end(); ?>

    </section>

    <section class="section section-event">



        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($eventForm, 'events')->label('Liczba imprez z HXS:')  ?>

            <?= Html::submitButton('Wykonaj obliczenia', ['class' => 'btn btn-info']) ?>
        <?php ActiveForm::end(); ?>
    </section>


    <?php if (strlen($scoreHTML) > 0) : ?>
        <section class="section section-result">
            <?= $scoreHTML ?>
            <div><?= Html::a('Pobierz PDF', $downloadLink, ['target' => '_blank', 'class' => 'btn btn-success']) ?></div>
        </section>
    <?php endif; ?>
</div>