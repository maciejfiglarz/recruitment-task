<?php

namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;

use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\helpers\Url;

use app\models\UploadForm;
use app\models\EventForm;
use yii\web\UploadedFile;
use app\helpers\SiteHelper;
use app\helpers\FileHelper;
use Dompdf\Dompdf;

/**
 * Site controller
 */
class SiteController extends Controller
{


    /**
     * Displays list and upload form.
     *
     * @return mixed
     */

    public function actionIndex()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            $uploadData = $model->upload();
            if ($uploadData['status']) {
                $this->redirect(array('site/edit', 'fileName' => $uploadData['fileName']));
            }
        }

        $viewParameters = [
            'model' => $model
        ];

        return $this->render('index',  $viewParameters);
    }


    /**
     * Displays edit page.
     *
     * @return mixed
     */

    public function actionEdit($fileName)
    {


        $siteHelper = new SiteHelper();
        $eventForm = new EventForm();

        $preparedRows = $siteHelper->prepareRows($fileName);
        $preparedHTML = '';


        if (Yii::$app->request->isPost) {

            $post = Yii::$app->request->post();
            
            if (isset($post['DynamicModel'])) {
           
                $preparedRows = $siteHelper->updateExcelFile($fileName, $post);

                Yii::$app->session->setFlash('success', 'Plik został zaktualizowany!');

            } else if (isset($post['EventForm'])) {
                $events = $post['EventForm']['events'];
                $eventForm->events =$events;

                $preparedHTML = $siteHelper->prepareHTML($events, $preparedRows, $fileName);

                Yii::$app->session->setFlash('success', 'Wygenerowano plik PDF!');
            }
        }


        $viewParameters = [
            'rows' => $preparedRows,
            'eventForm' => $eventForm,
            'scoreHTML' => $preparedHTML,
            'downloadLink' => Url::toRoute(['site/download', 'fileName' => $fileName]),
            'editForm' =>  $siteHelper->prepareEditModel($preparedRows)
        ];
        //dd($model->validate());
        return $this->render('edit', $viewParameters);
    }


    public function actionDownload($fileName = '')
    {

        $myfilepath = dirname(__FILE__) . '/../data/pdf/';

        $filecontent = file_get_contents($myfilepath . $fileName . '.pdf');

        header("Content-Type: application/pdf");

        header("Content-disposition: attachment; filename=$fileName");

        header("Pragma: no-cache");

        echo $filecontent;

        exit;
    }




}
