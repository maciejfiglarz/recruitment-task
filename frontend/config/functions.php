<?php

/**
 * Debug function
 * d($var);
 */
function dd($v) {
    \yii\helpers\VarDumper::dump($v, 10, true);
    exit();
}