<?php

namespace app\helpers;

use Yii;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader;
use app\helpers\FileHelper;
use yii\base\DynamicModel;

/**
 * Helper for Site
 */
class SiteHelper
{

    public function __construct()
    {
        $this->fileHelper = new FileHelper();
    }



    public function prepareEditModel(?array $preparedRows){

        $preparedRowsForValid = $this->prepareRowsForValid($preparedRows); 

        $model = DynamicModel::validateData($preparedRowsForValid['all'], [
            [array_values($preparedRowsForValid['integer']), 'integer', 'message' => 'Wpisz liczbę!'],
        ]);
        return $model;
    }

    public function prepareRowsForValid(?array $data): array
    {
        $preparedDataInteger = [];
        $preparedData = [];
        foreach ($data as $indexRow => $row) {
            foreach ($row as $indexCell => $cell) {
                if ($indexRow > 0 && $indexCell > 1) {
                    $preparedDataInteger[$indexRow . '_' . $indexCell] = $indexRow . '_' . $indexCell;
                }
                    $preparedData[$indexRow . '_' . $indexCell] = $cell;
            }
        }
        return ['integer' => $preparedDataInteger, 'all' => $preparedData];
    }

    public function prepareEventScore($events, $data)
    {
        $preparedArray = [];
        foreach ($data as $index => $row) {
            if ($index > 0) {
                
                $row[2] =$events * (int)$row[2];
                $row[3] =$events * (int)$row[3];
                $row[4] =$events * (int)$row[4];
            }


            array_push($preparedArray, $row);
        }

        return $preparedArray;
    }


    public function updateExcelFile(?string $fileName, ?array $post): array
    {

        $data = [];
        foreach($post['DynamicModel'] as $index => $cell){
            $explode = explode('_',$index);
            $data[$explode[0]][$explode[1]] = $cell;
        }
        
        $this->fileHelper->updateExcelFile($fileName, $data);

        $data = $this->prepareRows($fileName);

        return $data;
    }

    public function prepareHTML(?int $events, ?array $preparedRows, ?string $fileName): string
    {
        $data = $this->prepareEventScore($events, $preparedRows);

        $html = '<html>';
        $html .= '<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    <style>
                    *{ font-family: DejaVu Sans; font-size: 12px;}
                    body { font-family: Arial, Helvetica, sans-serif; }
                    table td,th { padding: 10px; }
                    </style>
                  </head>';
        $html .= "<body><table class='table'>";
        foreach ($data as $index => $row) {
            $html .= "<tr>";
            foreach ($row as $cell) {
                if ($index == 0) {
                    $html .= '<th>' . $cell . '</th>';
                } else {
                    $html .= '<td>' . $cell . '</td>';
                }
            }
            $html .= "</tr>";
        }
        $html .= '</table>';

        $html .= '<body></html>';


        $this->fileHelper->renderPdf($html, $fileName);
        return $html;
    }


    public function prepareRows(?string $fileName): array
    {
        $data = $this->fileHelper->getExcelFile($fileName);

        $data =  $this->removeEmptyRows($data);
        $data =  $this->removeEmptyColumns($data);

        return $data;
    }


    private function removeEmptyColumns($data)
    {
        $preparedData = [];
        $numberOfColumns = $this->countActiveColumns($data);

        foreach ($data as $rowIndex => $row) {
            foreach ($row as $cellIndex => $cell) {
                if (!$numberOfColumns || $cellIndex < $numberOfColumns) {
                    $preparedData[$rowIndex][$cellIndex] = $cell;
                }
            }
        }
        return $preparedData;
    }

    private function countActiveColumns($data)
    {
        $firstRowArray = reset($data);
        return array_search(null, $firstRowArray);
    }

    private function removeEmptyRows($data)
    {
        return array_filter($data, function ($var) {
            if (!($var['0'] == '' || $var['0'] == null)) {
                return $var;
            }
        });
    }
}
