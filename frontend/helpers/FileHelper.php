<?php

namespace app\helpers;

use Yii;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader;
use Dompdf\Dompdf;

/**
 * Helper for Files
 */
class FileHelper
{


    public function getFileName(?string $fileName): string
    {
        $explode  = explode('.', $fileName);
        return reset($explode);
    }

    public function getUploadFilePath(?string $fileName): string
    {
        return Yii::$app->getBasePath() . '/data/uploads/' . $fileName . '.xlsx';
    }
    public function getPdfFilePath(?string $fileName): string
    {
        return Yii::$app->getBasePath() . '/data/pdf/' . $fileName . '.pdf';
    }


    public function updateExcelFile(?string $fileName, ?array $data): array
    {
        $filePath = $this->getUploadFilePath($fileName);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray($data, NULL, 'A1');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($filePath);

        return $data;
    }

    public function getExcelFile(?string $fileName): array
    {

        $filePath = $this->getUploadFilePath($fileName);
        $reader = IOFactory::createReader('Xlsx');
        $reader->setReadDataOnly(true);
        $reader->setReadEmptyCells(false);

        $spreadsheet = $reader->load($filePath);
        $worksheet = $spreadsheet->getActiveSheet();

        return $worksheet->toArray();
    }
    public function renderPdf(?string $html,?string $fileName): bool
    {
        $dompdf = new Dompdf();
        $html = mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8');
        $dompdf->set_option('defaultFont', 'DejaVu Sans');
        $dompdf->set_option('isRemoteEnabled', TRUE);

        $dompdf->setPaper('A4', 'landscape');

        $dompdf->loadHtml($html, 'UTF-8');



        $dompdf->render();
        $output = $dompdf->output();

        if (file_put_contents($this->getPdfFilePath($fileName), $output)) {
            return true;
        }
        return false;
    }
}
